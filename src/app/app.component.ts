import { Component } from '@angular/core';
import {SessionService} from './session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestión de películas';

  constructor(public sessionService: SessionService) {}

  logout() {
  	this.sessionService.destroy();
  }
}
