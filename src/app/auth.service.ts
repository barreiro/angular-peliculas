// src/app/auth.service.ts
import { Injectable } from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import {User} from './user';

@Injectable()
export class AuthService {

 private apiBaseUrl: string = environment.apiBaseUrl;
 private apiPasswordClient: number = environment.apiPasswordClient;
 private apiPasswordSecret: string = environment.apiPasswordSecret;
 private applicationToken: string = environment.applicationToken;

 constructor(private http: HttpClient) { }

 private handleError<T> (operation = 'operation', result?: T) {
	 return (error: any): Observable<T> => {
	 console.error(error);
	 return of(result as T);
	 };
 }

 checkLogin(user: string, password: string): Observable<object> {
	let credentials: object = {
		 grant_type: 'password',
		 client_id: this.apiPasswordClient,
		 client_secret: this.apiPasswordSecret,
		 username: user,
		 password: password,
		 scope: '*'
 	};

 	let httpOptions = {
	 headers: new HttpHeaders({ 'Content-Type': 'application/json',
	'Authorization': this.applicationToken})
 	};

	 let loginUrl: string = this.apiBaseUrl + '/oauth/token';
	 
	 return this.http.post(loginUrl, credentials, httpOptions)
	 .pipe(
	 catchError(this.handleError('checkLogin', {}))
	 );
 }

 getUserByToken(token: string): Observable<User> {
	 let userUrl: string = this.apiBaseUrl + '/api/user';
	 let httpOptions = {
	 	headers: new HttpHeaders({ 'Content-Type': 'application/json',
	'Authorization': token})
	 };
	 return this.http.get<User>(userUrl, httpOptions)
	 .pipe(
	 catchError(this.handleError('getUserByToken', new User(0, '',
	'')))
	 );
	}
}
