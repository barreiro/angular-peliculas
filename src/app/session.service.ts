import { Injectable } from '@angular/core';
import {User} from './user';
@Injectable()
export class SessionService {
 public user: User = JSON.parse(localStorage.getItem('session.user'));

 constructor() { }

 isLoggedIn(): boolean {
	 if (this.user && this.user.id>0) {
	 	return true;
	 }
	 return false;
 }

 getUser(): User {
 	return this.user;
 }

 setUser(user: User): void {
	 this.user = user;
	 localStorage.setItem('session.user', JSON.stringify(user));
 }

 getAccessToken(): string {
 	return this.getUser().accessToken;
 }

 setAccessToken(token: string): void {
	 this.getUser().accessToken = token;
	 localStorage.setItem('session.user',
	JSON.stringify(this.getUser()));
 }

 destroy(): void {
 	this.setUser(null);
 }
}
