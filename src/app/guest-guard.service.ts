import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {SessionService} from './session.service';
@Injectable()
export class GuestGuardService implements CanActivate {

 constructor(
 	private sessionService: SessionService, 
 	private router:Router
 	) { }
 
 canActivate(): boolean {
	 if (this.sessionService.isLoggedIn()) {
		 this.router.navigate(['']);
		 return false;
	 }
	 return true;
 }
}
