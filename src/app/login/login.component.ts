import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {User} from '../user';
import {SessionService} from '../session.service';
import { Router } from '@angular/router';

@Component({
 selector: 'app-login',
 templateUrl: './login.component.html',
 styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 public credentials = {
	 email : '',
	 password : ''
 };

 public loading = false;
 public errorMessage = '';

 constructor(
 	private authService: AuthService, 
 	public sessionService: SessionService,
 	public router: Router) { }

 ngOnInit() {
 }

 login() {
 	 this.errorMessage = '';
	 this.loading = true;
	 this.authService.checkLogin(this.credentials.email, this.credentials.password)
	 	.subscribe( response => this.processLoginResponse(response));
 }

 processLoginResponse(response) {
	 if (response.access_token) {
	 	let accessToken = `${response.token_type} ${response.access_token}`;
		 this.authService.getUserByToken(accessToken)
		 .subscribe(user => this.processUserResponse(user, accessToken));
	 } else {
		 this.loading = false;
		 this.errorMessage = 'Error en autenticación';
	 }
 }

 processUserResponse(user: User, accessToken: string) {
	 if (user.id > 0) {
	 	user.accessToken = accessToken;
 		this.sessionService.setUser(user);
 		this.router.navigate(['']);
	 } else {
	 	this.errorMessage = 'Error al obtener datos del usuario';
	 }
	 this.loading = false;
 }

}